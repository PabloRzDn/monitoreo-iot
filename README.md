# Proyecto módulo de monitoreo de la condición Humedad y temperatura
---

### Nivel de madurez tecnológica actual: TR3

Función crítica analítica y experimental y/o prueba de concepto característica.

---






### Alcance

El alcance de este proyecto consta de un desarrollo de ingeniería conceptual, básica, detalle e implementación del diseño de un módulo IoT con sensores para el control y monitoreo de la condición, pensado inicialmente para medir variables de humedad y temperatura. El proyecto contempla la especificación y diseño de los módulos a utilizar, considerando una implementación  modular.

### Descripción de la solución

Utilizando la red Wifi del lugar en el cual se encuentra la compostera en cuestión, se medirán y enviarán datos de humedad y temperatura en tiempo real al servidor Thinger.io. Esto permitirá al usuario controlar las variables más importantes para la salud del compost. Asimismo, esta conexión de datos inalámbrica permitirá enviar alertas al usuario, respecto a si el sustrato se encuentra en las condiciones ideales de temperatura y humedad, manejar bases de datos, geolocalización, entre otros. También, se adjuntan a este repositorio la conexión con distintas plataformas IoT, tales como Blynk y Thingsboard.


