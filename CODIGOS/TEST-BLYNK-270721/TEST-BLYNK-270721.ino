#include "DHT.h"
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>


char auth[]="VNmLAe7V6I1sMdybKEEvx945Idh65tKh"; //token de autorización para Blynk

//credenciales red WiFi

char ssid[]="TERSA"; //nombre de la red
char password[]="Tersa-WIFI"; //contraseña de la red

BlynkTimer timer; //se crea objeto timer. Envía cada un segundo el valor a un pin virtual de blynk


#define DHTPIN 14 //se define pin de señal para dht
#define DHTTYPE DHT22 //se define tipo de sensor dht

float hum;

DHT dht(DHTPIN,DHTTYPE);



void timerEvento(){
  hum=dht.readHumidity();
  Serial.println(hum);
  Blynk.virtualWrite(V0,hum);
  
  
}


void setup() {
  Serial.begin(115200);
  dht.begin();
  Blynk.begin(auth,ssid,password);
  timer.setInterval(5000,timerEvento);
}

void loop() {
  Blynk.run();
  timer.run();

}
