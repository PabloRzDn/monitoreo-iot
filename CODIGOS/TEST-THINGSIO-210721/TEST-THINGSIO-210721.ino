#define _DEBUG_

#include <OneWire.h>
#include <Wire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <ThingerESP8266.h>
#include <Adafruit_SSD1306.h>


/***************DEFINICIÓN VARIABLES THINGER.IO***************/


#define USER "urbanatika_admin"
#define DEVICE_ID "iotcompost001"
#define CREDENTIALS "?6_o2HOM?ukP@jQO"

/***************DEFINICIÓN PINES LED_RGB***************/


#define LED_R 14 //pin led rojo
#define LED_G 12 //pin led verde
#define LED_B 13 //pin led azul

/***************DEFINICIÓN SENSOR DHT***************/

#define ONEWIRE 2 //se define pin de señal para sensor OneWire
float lectura_temp; //variable que almacena la temperatura medida 


/************ DEFINICIÓN SENSOR ANÁLOGO DE HUMEDAD ***********/

#define SENSOR_HUMEDAD 0
float lectura_hum=0.0; //variable que almacena la humedad medida
float transf_hum=0.0;//variable que trnasforma la humedad medida
float escal_hum=0.0; //variable de escalamiento de humedad 

/***************DEFINICIÓN VARIABLES LATITUD Y LONGITUD***************/
 
//const float LAT=-33.41908;
//const float LONG=-70.64169;

/***************DEFINICIÓN VARIABLES PANTALLA OLED***************/

#define ANCHO_PANTALLA 128
#define ALTO_PANTALLA 64

/***************DEFINICIÓN VARIABLES ICONOS Y COLORES PANTALLA OLED***************/

#define ANCHO_BITMAP_LOGO   60
#define ALTO_BITMAP_LOGO    60

#define ANCHO_BITMAP_ICONOS_GRANDES   20
#define ALTO_BITMAP_ICONOS_GRANDES    20

#define ANCHO_BITMAP_ICONOS_PEQUENOS   10
#define ALTO_BITMAP_ICONOS_PEQUENOS    10

#define BLUE            0x001F
#define RED             0xF800
#define GREEN           0x07E0


/***************DEFINICIÓN VARIABLES FUNCIÓN MAP DE HUMEDAD***************/

const int CONST_AIRE=552;
const int CONST_TSECA=541;
const int CONST_THUMEDA=359;



/*#################### IMAGENES Y LOGOS ####################*/

/*********** LOGO URBANATIKA ***********/



// '_C4z4ysp_400x400', 60x60px
const unsigned char logo_urbanatika_C4z4ysp_400x400 [] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xff, 0xc0, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x1f, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7c, 0x00, 0x7c, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x70, 0x10, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0x87, 0x80, 0x00, 0x00, 
  0x00, 0x03, 0x03, 0xff, 0xe1, 0xc0, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0xf8, 0xe0, 0x00, 0x00, 
  0x00, 0x07, 0xf8, 0x00, 0x3c, 0x70, 0x00, 0x00, 0x00, 0x07, 0x3e, 0x3f, 0x0e, 0x30, 0x00, 0x00, 
  0x00, 0x07, 0x0f, 0x1f, 0xc7, 0x38, 0x00, 0x00, 0x00, 0x07, 0x03, 0x81, 0xe3, 0x98, 0x00, 0x00, 
  0x00, 0x07, 0x01, 0xc0, 0x71, 0x8c, 0x00, 0x00, 0x00, 0x07, 0x00, 0xe0, 0x39, 0x80, 0x00, 0x00, 
  0x00, 0x07, 0x00, 0x60, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x70, 0x08, 0x3e, 0x00, 0x00, 
  0x00, 0x07, 0x00, 0x30, 0x01, 0xfe, 0x00, 0x00, 0x00, 0x07, 0x00, 0x30, 0x03, 0xe6, 0x00, 0x00, 
  0x00, 0x07, 0x00, 0x38, 0x0f, 0x06, 0x00, 0x00, 0x00, 0x07, 0x00, 0x18, 0x1e, 0x06, 0x00, 0x00, 
  0x00, 0x07, 0x00, 0x18, 0x38, 0x06, 0x00, 0x00, 0x00, 0x07, 0x00, 0x18, 0x30, 0x06, 0x00, 0x00, 
  0x00, 0x07, 0x00, 0x18, 0x70, 0x06, 0x00, 0x00, 0x00, 0x07, 0x00, 0x18, 0x60, 0x0e, 0x00, 0x00, 
  0x00, 0x03, 0x00, 0x18, 0xe0, 0x0e, 0x00, 0x00, 0x00, 0x03, 0x00, 0x18, 0xc0, 0x0e, 0x00, 0x00, 
  0x00, 0x03, 0x00, 0x18, 0xc0, 0x0c, 0x00, 0x00, 0x00, 0x03, 0x00, 0x19, 0xc0, 0x0c, 0x00, 0x00, 
  0x00, 0x03, 0x00, 0x19, 0xc0, 0x0c, 0x00, 0x00, 0x00, 0x01, 0x80, 0x19, 0xc0, 0x1c, 0x00, 0x00, 
  0x00, 0x01, 0x80, 0x19, 0xc0, 0x18, 0x00, 0x00, 0x00, 0x01, 0xc0, 0x19, 0x80, 0x38, 0x00, 0x00, 
  0x00, 0x00, 0xe0, 0x19, 0x80, 0x30, 0x00, 0x00, 0x00, 0x00, 0x70, 0x19, 0x80, 0x60, 0x00, 0x00, 
  0x00, 0x00, 0x38, 0x19, 0x80, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x19, 0x83, 0xc0, 0x00, 0x00, 
  0x00, 0x00, 0x0f, 0x99, 0x8f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xf9, 0xfe, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xf9, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

// Array of all bitmaps for convenience. (Total bytes used to store images in PROGMEM = 496)
const int logo_urbanatikaallArray_LEN = 1;
const unsigned char* logo_urbanatikaallArray[1] = {
  logo_urbanatika_C4z4ysp_400x400
};



/*********** ICONO HUMEDAD ***********/

// 'humidity-svgrepo-com', 20x20px
const unsigned char icono_humedadhumidity_svgrepo_com [] PROGMEM = {
  0x00, 0x60, 0x00, 0x00, 0xf0, 0x00, 0x00, 0xf8, 0x00, 0x03, 0x7c, 0x00, 0x03, 0x7c, 0x00, 0x06, 
  0xfe, 0x00, 0x06, 0xfe, 0x00, 0x04, 0xfe, 0x00, 0x0b, 0xff, 0x00, 0x0b, 0xff, 0x00, 0x13, 0xff, 
  0x80, 0x37, 0xff, 0xc0, 0x3f, 0xff, 0xc0, 0x3f, 0xff, 0xc0, 0x37, 0xff, 0xc0, 0x3f, 0xff, 0xc0, 
  0x0f, 0xff, 0x00, 0x0f, 0xff, 0x00, 0x07, 0xfe, 0x00, 0x00, 0xfc, 0x00
};

// Array of all bitmaps for convenience. (Total bytes used to store images in PROGMEM = 80)
const int icono_humedadallArray_LEN = 1;
const unsigned char* icono_humedadallArray[1] = {
  icono_humedadhumidity_svgrepo_com
};

/*********** ICONO TEMPERATURA ***********/
// 'thermometer-temperature-svgrepo-com', 20x20px
const unsigned char icono_temperaturathermometer_temperature_svgrepo_com [] PROGMEM = {
  0x00, 0xf0, 0x00, 0x01, 0x08, 0x00, 0x01, 0x08, 0x00, 0x01, 0x08, 0x00, 0x01, 0x08, 0x00, 0x01, 
  0x08, 0x00, 0x01, 0x08, 0x00, 0x01, 0x08, 0x00, 0x01, 0x08, 0x00, 0x00, 0x00, 0x00, 0x04, 0x02, 
  0x00, 0x08, 0xf1, 0x00, 0x08, 0xf1, 0x00, 0x09, 0xf9, 0x00, 0x09, 0xf9, 0x00, 0x09, 0xf9, 0x00, 
  0x08, 0xf1, 0x00, 0x04, 0x02, 0x00, 0x04, 0x02, 0x00, 0x01, 0xf8, 0x00
};

// Array of all bitmaps for convenience. (Total bytes used to store images in PROGMEM = 80)
const int icono_temperaturaallArray_LEN = 1;
const unsigned char* icono_temperaturaallArray[1] = {
  icono_temperaturathermometer_temperature_svgrepo_com
};



/*********** ICONO WIFI ***********/

// 'wi-fi-svgrepo-com', 10x10px
const unsigned char icono_wifiwi_fi_svgrepo_com [] PROGMEM = {
  0x00, 0x00, 0x1e, 0x00, 0x7f, 0x80, 0xc0, 0xc0, 0x1e, 0x00, 0x33, 0x00, 0x00, 0x00, 0x0c, 0x00, 
  0x0c, 0x00, 0x00, 0x00
};

// Array of all bitmaps for convenience. (Total bytes used to store images in PROGMEM = 48)
const int icono_wifiallArray_LEN = 1;
const unsigned char* icono_wifiallArray[1] = {
  icono_wifiwi_fi_svgrepo_com
};



/***************OBJETO DE LA CLASE ADAFRUIT_SSD1306***************/

Adafruit_SSD1306 display(ANCHO_PANTALLA,ALTO_PANTALLA,&Wire, -1);


/***************CREDENCIALES RED WIFI***************/

char ssid[]="Redmi 9"; //nombre de la red
char password[]="12345678"; //contraseña de la red



/********** CLASE DE ESTADOS DE RED Y CONEXIÓN A THINGER.IO  **********/

class ClientListener : public ThingerESP8266{ //Se crea clase ClientListener, que llama a los estados de conexión y de thinger.io
public:
    ClientListener(const char* user, const char* device, const char* device_credential) : 
      ThingerESP8266(user, device, device_credential){}
protected:
  virtual void thinger_state_listener(THINGER_STATE state){
    // llama a la implementación actual (debug)
    ThingerESP8266::thinger_state_listener(state);
    switch(state){
        case THINGER_AUTHENTICATED: //caso 1: verificación de thinger autenticado
            digitalWrite(LED_G,HIGH);
            delay(500);
            digitalWrite(LED_G,LOW);
            break;
        case NETWORK_CONNECT_ERROR: //caso 2: errores de conexión a la red 
            digitalWrite(LED_R,HIGH);
            delay(500);
            digitalWrite(LED_R,LOW);
            
            break;
        default:
            digitalWrite(LED_B,HIGH);
            delay(200);
            digitalWrite(LED_B,LOW);
            delay(200);
            digitalWrite(LED_B,HIGH);
            delay(200);
            digitalWrite(LED_B,LOW);
            
            break;
    }
  }
};

ClientListener thing(USER, DEVICE_ID, CREDENTIALS);




/***************OBJETO SENSOR DS18B20***************/


OneWire ourWire(ONEWIRE); //se establece pin como bus para datos de sensor
DallasTemperature sensors(&ourWire); //se llama a librería DallasTemperature


/*################### DECLARACIÓN DE FUNCIONES ###################*/

float EscalamientoHumedad(float humedad){ //función que recibe como parámetro la lectura análoga y la escala a humedad (0%-100%)
  transf_hum=constrain(humedad,CONST_THUMEDA,CONST_TSECA);
  transf_hum=map(transf_hum,CONST_THUMEDA,CONST_TSECA,100,0);
  return transf_hum;   
}

void LogoUrbanatika(void){
  display.clearDisplay();
 
  display.setTextSize(1.7);
  display.setTextColor(WHITE);
  display.setCursor(15,0);
  display.print("Urbanatika IoT");
  display.drawBitmap(25,5,logo_urbanatika_C4z4ysp_400x400,ANCHO_BITMAP_LOGO,ALTO_BITMAP_LOGO,WHITE);
  display.display();

  //display.clearDisplay();
  display.setTextSize(1.7);
  display.setTextColor(WHITE);
  display.setCursor(25,56);
  display.print("Iniciando...");

  display.display();
  delay(1000);
 
}


void MostrarVariables(float humedad, float temperatura){

  
  display.clearDisplay();

  display.drawBitmap(28,18,icono_temperaturathermometer_temperature_svgrepo_com,ANCHO_BITMAP_ICONOS_GRANDES,ALTO_BITMAP_ICONOS_GRANDES,WHITE);
  display.drawBitmap(28,43,icono_humedadhumidity_svgrepo_com,ANCHO_BITMAP_ICONOS_GRANDES,ALTO_BITMAP_ICONOS_GRANDES,WHITE);
  display.drawBitmap(110,0,icono_wifiwi_fi_svgrepo_com,ANCHO_BITMAP_ICONOS_PEQUENOS,ALTO_BITMAP_ICONOS_PEQUENOS,WHITE);

  display.setTextSize(1.7);
  display.setTextColor(WHITE);
  display.setCursor(15,0);
  display.print("Urbanatika IoT");
  
  display.setTextSize(1.7);
  display.setTextColor(WHITE);  
  display.setCursor(50,25);
  display.print(temperatura);
  display.print(" C");

  display.setTextSize(1.7);
  display.setTextColor(WHITE);
  display.setCursor(50,50);
  display.print(humedad);
  display.print(" %");
  
  display.display();
  delay(2000);
  display.clearDisplay();
  
  
}

float PromedioHumedad(float humedad){
  float valor_sensor=0;
  for (int i=0; i<=50; i++){
    valor_sensor=valor_sensor+analogRead(humedad);
    delay(1);
  }
  valor_sensor=valor_sensor/50.0;
  return valor_sensor;
}


void setup() {
  
  
  //declaración de entradas
  pinMode(ONEWIRE,INPUT);
  pinMode(SENSOR_HUMEDAD,INPUT);

  //declaración de salidas
  pinMode(LED_R,OUTPUT);
  pinMode(LED_G,OUTPUT);
  pinMode(LED_B,OUTPUT);
  
  

  Serial.begin(115200);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  delay(1000);
  display.clearDisplay();
  
  LogoUrbanatika();

  
  thing.add_wifi(ssid,password); //se conecta a red Wifi utilizando método de thinger.io
  

  thing["esp8266"]>>[](pson& out){ 
    
    sensors.requestTemperatures(); //método para realizar lecturas 
    lectura_temp=sensors.getTempCByIndex(0); //se almacena la temperatura leída en °C

    
    out["Temperatura"]=lectura_temp;
    Serial.println(lectura_temp);
    
    
    lectura_hum=PromedioHumedad(SENSOR_HUMEDAD); //se almacena la humedad leída
    escal_hum=EscalamientoHumedad(lectura_hum);
    out["Humedad"]=escal_hum;
   
    
    
    Serial.println(escal_hum);
    Serial.println(lectura_hum);
    MostrarVariables(escal_hum,lectura_temp);
    
    
    digitalWrite(LED_B,HIGH);
    delay(500);
    digitalWrite(LED_B,LOW);
    
    
  };

  //thing["ubicacion"]>>[](pson& out){
   // out["latitud"]=LAT;
   // out["longitud"]=LONG;
    
    //}; 

    sensors.begin();
}

void loop() {
  
  thing.handle();
  
}
