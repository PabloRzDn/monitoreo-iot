int pin = 8;
int sensor = A0;
int sensorValue = 0;
void setup () {
  Serial.begin(9600);
  pinMode(pin,OUTPUT);
  
}
void loop(){
  sensorValue = analogRead(sensor);
  Serial.println(sensorValue, DEC);
  if (sensorValue > 130) {
    digitalWrite(pin, HIGH); 
  }
else {
  digitalWrite(pin, LOW);
}
}
