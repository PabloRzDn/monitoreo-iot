#include "DHT.h"
#include <ESP8266WiFi.h>
#include "ThingsBoard.h"


//**********credenciales red WiFi**********

char ssid[]="TERSA"; //nombre de la red
char password[]="Tersa-WIFI"; //contraseña de la red

//**********Credenciales para ThingsBoard**********

#define TOKEN "ZbX49Yif3vhEaZ7xusAs"
#define THINGSBOARD_SERVER "thingsboard.cloud"


//**********DHT22***********

#define DHTPIN 14 //se define pin de señal para dht
#define DHTTYPE DHT22 //se define tipo de sensor dht
float hum; //variable para lectura de humedad en dht
float temp; //variable para lectura de temperatura dht
DHT dht(DHTPIN,DHTTYPE);

//**********Tiempo de intervalos***********

unsigned long millisPrevio=0; //variable que toma el tiempo previo de intervalos para medición de variables
const long intervalo=15000; //intervalo (en milisegundos) de medición de variables


//***********Inicialización comunicación ThingsBoard***********

WiFiClient espClient; //objeto espClient de clase WiFiClient
ThingsBoard tb(espClient); //objeto tb que recibe como parámetro el objeto espClient
int status=WL_IDLE_STATUS; //asignación de valor a la variable status


void setup() { 
  Serial.begin(115200); //inicialización puerto serial recibiendo como parámetro el Baudrate
 
  
  WiFi.begin(ssid,password); //inicialización WiFi, recibiendo ssid y password
  dht.begin(); //inicialización dht
  InitWiFi();
  
}
void loop() {
  
  
  unsigned long millisActual=millis();

  
  
  if(WiFi.status()!=WL_CONNECTED){ //comprobación: si status de wifi es distinto al conectado, se inicia la función reconnect
    reconnect();
  }

  if(!tb.connected()){ 
    Serial.print("Conectado a: ");
    Serial.println(THINGSBOARD_SERVER);
  }
    
    if(millisActual-millisPrevio>intervalo){ //si la diferencia entre el millis actual y el previo es mayor a 15000 milisegundos
    millisPrevio=millisActual; //actualización variable millis() para el intervalo
    hum=dht.readHumidity(); //lectura humedad DHT
    temp=dht.readTemperature(); //lectura temperatura DHT
    Serial.print("Humedad: ");
    Serial.print(hum);
    Serial.print(" % ;");
    Serial.print(" Temperatura: ");
    Serial.print(temp);
    Serial.println(" °C");
    tb.sendTelemetryFloat("data humedad",hum);
    tb.sendTelemetryFloat("data temperatura",temp);
    tb.loop();
    }
    
    
  

    
    if(!tb.connect(THINGSBOARD_SERVER,TOKEN)){
      Serial.println("fallo al conectar");
      return;
      
    
  }
  

  
  


}



void InitWiFi(){
  WiFi.begin(ssid,password);
  while(WiFi.status()!=WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("Conectado a red");
  
  }
void reconnect(){
  status=WiFi.status();
  if(status!=WL_CONNECTED){
    WiFi.begin(ssid,password);
    while(WiFi.status()!=WL_CONNECTED){
      delay(500);
      Serial.print(".");
    }
    Serial.println("Conectado");
  }
}
